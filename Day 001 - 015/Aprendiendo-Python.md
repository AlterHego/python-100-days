## Conociendo a Python

### Introducción a Python

#### Historia de Python

1.  Navidad de 1989: Guido von Rossum comenzó a escribir compiladores para el lenguaje Python.
2.  Febrero de 1991: nació el primer compilador de Python (también un intérprete), que se implementó en C (hubo implementaciones de Java y C # de Jython y IronPython, y otras implementaciones como PyPy, Brython, Pyston, etc.) ), puede llamar a la función de biblioteca del lenguaje C. En las primeras versiones, Python ya brindaba soporte para bloques de construcción como "clase", "función", "manejo de excepciones", y proporcionaba tipos de datos básicos como "lista" y "diccionario", mientras que soportaba módulos. Para crear aplicaciones.
3.  Enero de 1994: Python 1.0 se lanza oficialmente.
4.  16 de octubre de 2000: La versión Python 2.0 agrega soporte para la completa recolección de basura y brinda soporte para Unicode. Al mismo tiempo, todo el proceso de desarrollo de Python es más transparente, la influencia de la comunidad en el progreso del desarrollo se expande gradualmente y el ecosistema toma forma lentamente.
5.  3 de diciembre de 2008: se lanza Python 3.0, no es totalmente compatible con el código anterior de Python, pero debido a que todavía hay muchas compañías que usan Python 2.x en proyectos y operaciones, muchas de las caracteristicasnuevas de Python 3.x fueron portada más tarde a Python 2.6 / 2.7.

La versión de Python 3.7.x que estamos usando actualmente se lanzó en 2018. El número de versión de Python se divide en tres secciones, que tienen la forma de A.B.C. Donde A indica el número de versión grande, generalmente cuando la reescritura general, o el cambio no es compatible con versiones anteriores, aumenta A; B indica la actualización de la función, aumenta B cuando aparecen nuevas características; C indica pequeños cambios (por ejemplo: arreglar un cierto Error), aumente C siempre que haya un cambio. Si está interesado en la historia de Python, puede leer una publicación de blog titulada ["Una breve historia de Python"](https://www.cnblogs.com/vamei/archive/2013/02/06/2892628.html). 
###### Escrita en Chino pero que vale mucho la pena traducir

#### Los pros y contras de Python
Las ventajas de Python son muchas, simples y se pueden resumir de la siguiente manera.

1.  Simple y claro, solo hay una forma de hacer una cosa.
2.  La curva de aprendizaje es baja y Python es más fácil de usar que muchos otros lenguajes.
3.  Código abierto con una comunidad y un ecosistema fuertes.
4.  Lenguaje interpretativo, plataforma portátil.
5.  Es compatible con los paradigmas de programación convencionales (programación orientada a objetos y programación funcional).
6.  El código es altamente estandarizado y legible, adecuado para personas con limpieza de código
7.  Extensibilidad e incrustabilidad, puede llamar al código C / C ++, también puede llamar a Python en C / C ++.

Las deficiencias de Python se centran principalmente en los siguientes puntos.

1.  La eficiencia de ejecución es ligeramente menor, por lo que las tareas computacionalmente intensivas se pueden escribir en C / C ++.
2.  El código no se puede cifrar, pero ahora muchas compañías no venden software sino que venden servicios, y este problema se diluirá.
3.  Hay demasiados marcos para elegir durante el desarrollo (por ejemplo, hay más de 100 marcos web) y hay errores en las elecciones.


