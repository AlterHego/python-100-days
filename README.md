# Python 100 Days

Python 100 Days [Conceptos básicos hasta avanzados]
Link al curso original [escrito en Chino](https://github.com/jackfrued/Python-100-Days)

Autor: **骆昊** [ (https://github.com/jackfrued) ]

Traducción al Español: @AlterHego [no tengo conocimiento del lenguaje chino por lo que me apoyo del traductor de Google y algunas fraces tengo que adaptarlas]

Sobre las intenciones del autor:

*En la actualidad, su idea es establecer un grupo de discusión por dirección técnica y organizar personal especializado para administrar estos grupos. Los gerentes de grupo deben tener conocimiento profesional y tiempo relativamente suficiente para responder las preguntas planteadas por los socios pequeños. Por supuesto, pagará a estos gerentes, y si estan interesado en convertirse en gerentes, pueden hablar con el en privado. Espera que de esta manera, podan reunir a más desarrolladores excelentes de Python, por un lado, para crear una plataforma de comunicación y, por otro lado, dejar que los nuevos integrantes obtengan la forma de preguntar y aprender rápidamente. Continuará trabajando duro para hacer clases públicas en línea y actividades de intercambio técnico fuera de línea, y espera que todos ustedes continúen apoyándole. No es fácil de hacerlo, gracias por su apoyo, este dinero no se utilizará para comprar café, sino que se donará a una organización educativa [haga clic para comprender la situación de la donación](https://github.com/jackfrued/Python-100-Days/blob/master/%E6%9B%B4%E6%96%B0%E6%97%A5%E5%BF%97.md). Agradece al apoyo brindado por el Departamento de Enseñanza de Python en Beijing Qianfeng Internet Technology Co., Ltd. [Para la clase abierta](https://ke.qq.com/course/406954)*

**Área de aplicación de Python y análisis de situación laboral**

En pocas palabras, Python es un lenguaje "elegante", "claro" y "simple".

*  la curva de aprendizaje es baja, y los no profecionales pueden aprender facilmente.
*  Sistema de codigo abierto.
*  Lenguaje interpretado, buena portabilidad de su plataforma.
*  Programación orientado a objetos.
*  capacidad para ampliar su funcionalidad llamando al código C/C++
*  De código legible.

Python se usa actualmente en varias áreas populares.

*  Infraestructura en la nube - Python / Java / Go
*  DevOps - Python / Shell / Ruby / Go
*  Rastreador web - Python / PHP / C ++
*  Minería de análisis de datos - Python / R / Scala / Matlab
*  Aprendizaje automático: Python / R / Java / Lisp

Como desarrollador de Python, las principales areas de empleo incluyen:

*  Python server background development / game server development / data interface development engineer
*  Python automated operation and maintenance engineer
*  Python Data Analysis / Data Visualization / Big Data Engineer
*  Python crawler engineer
*  Python Chat Robot Development / Image Recognition and Vision Algorithm / Deep Learning Engineer:


La tabla a continuación muestra los requisitos de reclutamiento de Python y las clasificaciones salariales para las principales ciudades (a partir de mayo de 2018).
![alt text](img/python-top-10.png "Title Text")
![alt text](img/python-bj-salary.png "Title Text")
![alt text](img/python-salary-chengdu.png "Title Text")

### Varios consejos para principiantes:
*  Haz que el inglés sea tu idioma de trabajo.
*  La práctica hace la perfección.
*  Toda experiencia proviene de errores.
*  No seas una de las sanguijuelas.
*  O se destacan o son expulsados.

## Día 01 ~ 15 - Conceptos básicos del lenguaje Python

### Día01 - Conociendo Python

*  Introducción a Python - Historia de Python / Ventajas y desventajas de las áreas de aplicación Python / Python
*  Creación de un entorno de programación: entorno Windows / entorno Linux / entorno MacOS
*  Ejecute el programa Python desde la terminal - Hola, mundo / función de impresión / Ejecute el programa
*  Use IDLE - Entorno interactivo (REPL) / Escriba varias líneas de código / Ejecute programas / Salga de IDLE
*  Comentarios: la función de los comentarios / comentarios de una sola línea / comentarios de varias líneas
  

### Día 02 - Elementos del lenguaje

*  Programa y Binario - Instrucciones y Programas / Von Neumann / Binario y Decimal / Octal y Hexadecimal
*  Variables y tipos - denominación de variables / uso de variables / funciones de entrada / comprobación de tipos de variables / conversión de tipos de variables
*  Números y cadenas - enteros / flotantes / plurales / cadenas / operaciones básicas de cadena / codificación de caracteres
*  Operador - Operador matemático / Operador de asignación / Operador de comparación / Operador lógico / Operador de identidad / Prioridad del operador
*  Ejemplo de aplicación - Convertir temperatura Fahrenheit a Celsius / Ingrese el radio del círculo para calcular el perímetro y el área / Ingrese el año para determinar si es un año bisiesto


### Día03 - estructura de la rama

*  Escenario de aplicación de la estructura - condición / sangría / bloque de código / diagrama de flujo
*  Sentencia if - estructura simple if / if-else / estructura if-elif-else / if anidada
*  Caso de aplicación: Autenticación de usuario / Intercambio entre unidades métricas y en inglés / Qué hacer con los dados / El sistema del sistema porcentual / La evaluación de la función por partes / Ingrese la longitud de los tres lados; Si puede formar un triángulo, calcule la circunferencia y el área.


# Conforme tenga el tiempo voy a ir subiendo todo el contenido de los 100 Dias.
## Espero les sea de utilidad y tengan paciencia.
### Continuara...

